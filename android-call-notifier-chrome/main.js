// Android Call Notifier - Chrome Extension
// Author: Matti Määttä 2014 (maatta.matti at gmail.com)

$(function() {
    // Set values from settings
    chrome.storage.local.get('registered', function(result) {
        $('#registrationId').html(result['registered']);
    });

    chrome.storage.local.get('deviceName', function(result) {
        $('#deviceName').val(result['deviceName']);
    });

    $('#deviceName').keyup(function(ev) {
        var name = $(this).val();
        chrome.storage.local.set({deviceName: name}, function(result) {
            console.log('Device name set to', name);
        });
    });
});
