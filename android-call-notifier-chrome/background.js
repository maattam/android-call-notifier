// Android Call Notifier - Chrome Extension
// Author: Matti Määttä 2014 (maatta.matti at gmail.com)

// GCM service identifier
var SERVICE_ID = '';

// Unique identifier used for handshaking
var APP_ID = 'c037d506-546f-43f9-9889-5df2fb2b52db';

var ADVERTISE_PORT = 6832;
var ADVERTISE_GROUP = '229.1.2.3';
var LISTEN_PORT = 6830;

var ICONS = {
    'default':  'default.png',
    'call':     'google_dialer_icon.png',
    'sms':      'hangouts.png',

    // External apps
    'com.facebook.android':     'facebook.png',
    'com.facebook.katana':      'facebook.png'
};

var udp = chrome.sockets.udp;

function getIconUrl(iconName) {
    var icon = ICONS[iconName] || ICONS['default'];
    return 'icons/' + icon;
}

function showNotification(info) {
    chrome.notifications.create(info.id, {
        type:           'basic',
        title:          info.title,
        message:        info.message,
        eventTime:      parseInt(info.timestamp),
        contextMessage: info.contextMessage,
        iconUrl:        info.iconData || getIconUrl(info.icon)
    }, function(id) {
        console.log('Created notification with id ' + id);
    });
}

function registerCallback(registrationId) {
    if (chrome.runtime.lastError) {
        console.error('Error registering gcm service ', chrome.runtime.lastError);
        return;
    }

    chrome.storage.local.set({'registered': registrationId}, function() {
        console.log('Registered with new id ' + registrationId);
    });

    // Also set initial device name
    chrome.storage.local.set({'deviceName': 'My Chrome'});
}

function registerServices() {
    chrome.storage.local.get('registered', function(result) {
        // If extension is already registered, bail out
        if (result['registered']) {
            console.log('Registered with id ' + result['registered']);
        } else {
            // Register with the gcm service
            chrome.gcm.register([SERVICE_ID], registerCallback);
        }
    });
}

function collectAdvertisements(callback) {
    udp.create({}, function(sockInfo) {
        var socketId = sockInfo.socketId;

        udp.bind(socketId, '0.0.0.0', ADVERTISE_PORT, function(result) {
            if (result < 0) {
                console.log('Bind failed', result);
                return;
            }

            udp.joinGroup(socketId, ADVERTISE_GROUP, function(result) {
                if (result < 0) {
                    console.log('JoinGroup failed', result);
                } else {
                    console.log('Began collecting advertisements');
                }
            });
        });
    });

    udp.onReceive.addListener(function(info) {
        var stringData = String.fromCharCode.apply(null, new Uint8Array(info.data));
        console.log('Received', stringData);

        try {
            var advertisement = JSON.parse(stringData);
            if (advertisement['app_id'] === APP_ID) {
                callback({
                    advertisement: advertisement,
                    send: createResponseHandler(info)
                });
            }

        } catch (e) {
            console.err('Failed to parse advertisement', e);
        }
    });
}

function createResponseHandler(info) {
    return function(data) {
        var json = JSON.stringify(data);

        var bytes = new ArrayBuffer(json.length);
        var bufferView = new Uint8Array(bytes);
        for (var i = 0; i < json.length; ++i) {
            bufferView[i] = json.charCodeAt(i);
        }

        // Create socket for response
        udp.create({}, function(sockInfo) {
            udp.bind(sockInfo.socketId, '0.0.0.0', 0, function(result) {
                if (result < 0) {
                    console.log('Failed to bind sender socket', result);
                    udp.close(sockInfo.socketId);
                    return;
                }

                udp.send(sockInfo.socketId, bytes, info.remoteAddress, LISTEN_PORT, function(result) {
                    if (result < 0) {
                        console.log('Send failed', result);
                    } else {
                        console.log('Sent', json);
                    }

                    udp.close(sockInfo.socketId);
                });
            });
        });
    };
}

chrome.gcm.onMessage.addListener(function(message) {
    console.log(message);

    if (message.data['revoke_authorization']) {
        console.log('TODO: revoke_authorization', message.data['revoke_authorization']);
    } else {
        showNotification(message.data);
    }
});

chrome.runtime.onStartup.addListener(registerServices);
chrome.runtime.onInstalled.addListener(registerServices);

// Begin collecting advertisements
collectAdvertisements(function(info) {
    // Reply back with our id
    chrome.storage.local.get(['registered', 'deviceName'], function(result) {
        if (result['registered']) {
            info.send({
                'app_id': APP_ID,
                'registration_id': result['registered'],
                'device': result['deviceName']
            });
        }
    });
});