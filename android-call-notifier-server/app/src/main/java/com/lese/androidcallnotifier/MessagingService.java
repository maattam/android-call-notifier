package com.lese.androidcallnotifier;

import org.json.JSONObject;

/**
 * Messaging service interface for throwing messages in the air.
 *
 * Created by Matti Määttä on 19.7.2014.
 */
public interface MessagingService {
    /**
     * Broadcasts data to all registered clients.
     *
     * @param data Message payload in JSONObject format
     * @param ttl Message time to live in seconds
     */
    void send(JSONObject data, int ttl);
}
