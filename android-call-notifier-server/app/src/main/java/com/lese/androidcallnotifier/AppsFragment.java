package com.lese.androidcallnotifier;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * Created by Matti Määttä on 24.7.2014.
 */
public class AppsFragment extends Fragment {

    private NotifiersProvider mNotifiersProvider;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        DatabaseOpenHelper dbHelper = new DatabaseOpenHelper(getActivity());
        mNotifiersProvider = new NotifiersProvider(dbHelper.getWritableDatabase());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.apps_view, container, false);

        ListView appsView = (ListView)view.findViewById(R.id.apps_list_view);
        final PackageManager pm = getActivity().getPackageManager();

        List<ApplicationInfo> apps = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        Collections.sort(apps, new Comparator<ApplicationInfo>() {
            @Override
            public int compare(ApplicationInfo appInfoA, ApplicationInfo appInfoB) {
                return pm.getApplicationLabel(appInfoA).toString().compareTo(pm.getApplicationLabel(appInfoB).toString());
            }
        });

        appsView.setAdapter(new ApplicationsAdapter(apps));

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.apps, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_setup) {
            Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private class ApplicationsAdapter extends ArrayAdapter<ApplicationInfo> {
        private Set<String> mNotifiers;

        private ApplicationsAdapter(List<ApplicationInfo> objects) {
            super(getActivity(), R.layout.item_app, objects);

            mNotifiers = new HashSet<>(mNotifiersProvider.getNotifiers());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ApplicationInfo item = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_app, parent, false);
            }

            ImageView imageView = (ImageView)convertView.findViewById(R.id.app_image);
            imageView.setImageDrawable(getActivity().getPackageManager().getApplicationIcon(item));

            TextView textView = (TextView)convertView.findViewById(R.id.app_name);
            textView.setText(getActivity().getPackageManager().getApplicationLabel(item));

            final CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.app_checkbox);
            checkBox.setChecked(mNotifiers.contains(item.packageName));
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (checkBox.isChecked()) {
                        mNotifiersProvider.addNotifier(item.packageName);
                        mNotifiers.add(item.packageName);
                    } else {
                        mNotifiersProvider.removeNotifier(item.packageName);
                        mNotifiers.remove(item.packageName);
                    }
                }
            });

            return convertView;
        }
    }
}
