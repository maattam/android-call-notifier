package com.lese.androidcallnotifier;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

/**
 * Factory class for creating new notifications.
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public class NotificationFactory {
    private final InstallationIdProvider mIdProvider;

    public NotificationFactory(InstallationIdProvider idProvider) {
        mIdProvider = idProvider;
    }

    /**
     * Constructs a new notification message. All parameters are required.
     *
     * @param icon ie. "default"
     * @return JSON notification data
     */
    public JSONObject create(String title, String message, String contextMessage, String icon) {
        JSONObject info = new JSONObject();

        try {
            info.put("id", UUID.randomUUID().toString());
            info.put("installation_id", mIdProvider.getInstallationId());
            info.put("title", title);
            info.put("message", message);
            info.put("contextMessage", contextMessage);
            info.put("timestamp", System.currentTimeMillis() / 1000L);
            info.put("icon", icon);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return info;
    }
}
