package com.lese.androidcallnotifier;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

/**
 * Listens for incoming phone calls and attempts to match phone numbers to contacts.
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public class PhoneCallListener extends PhoneStateListener {
    private final PhoneCallDelegate mDelegate;

    public PhoneCallListener(PhoneCallDelegate delegate) {
        mDelegate = delegate;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        if (state == TelephonyManager.CALL_STATE_RINGING) {
            mDelegate.ringing(incomingNumber);
        }
    }
}
