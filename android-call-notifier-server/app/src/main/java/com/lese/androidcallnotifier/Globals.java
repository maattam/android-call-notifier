package com.lese.androidcallnotifier;

/**
 *
 * Created by Matti Määttä on 19.7.2014.
 */
public class Globals {
    public static final int ADVERTISE_PORT = 6832;
    public static final int LISTEN_PORT = 6830;
    public static final String ADVERTISE_GROUP = "229.1.2.3";
    public static final int TTL = 1;

    public static final String PREFS_NAME = "CallNotifierPrefs";

    // Place GCM public API key from Developer Console here
    public static final String GCM_API_KEY = "";
}
