package com.lese.androidcallnotifier;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Maps registrations from JSON to SQL and back.
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public class RegistrationsProvider {
    private static String TAG = "RegistrationsProvider";

    final private SQLiteDatabase mDb;

    public RegistrationsProvider(SQLiteDatabase db) {
        mDb = db;
    }

    /**
     * @return JSON with keys "registration_id" and "device"
     */
    List<JSONObject> getRegistrations() {
        List<JSONObject> registrations = new ArrayList<>();

        Cursor cursor = mDb.query(DatabaseOpenHelper.REGISTRATIONS_TABLE_NAME,
                new String[] { "registrationId", "device" },
                null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                ContentValues row = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, row);

                JSONObject registration = new JSONObject();

                try {
                    registration.put("registration_id", row.get("registrationId"));
                    registration.put("device", row.get("device"));

                    registrations.add(registration);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } while (cursor.moveToNext());
        }

        cursor.close();
        return registrations;
    }

    /**
     * @param registration JSON with keys "registration_id" and "device"
     */
    public boolean addRegistration(JSONObject registration) {
        try {
            ContentValues row = new ContentValues();
            row.put("registrationId", registration.getString("registration_id"));
            row.put("device", registration.getString("device"));

            mDb.insertOrThrow(DatabaseOpenHelper.REGISTRATIONS_TABLE_NAME, null, row);
        }

        catch (SQLiteConstraintException e) {
            // This is ok because we can ignore duplicate entries from advertisements
            Log.v(TAG, "Ignoring already registered id");
            return false;
        }

        catch (JSONException e) {
            e.printStackTrace();
        }

        return true;
    }

    public void removeRegistration(String registrationId) {
        mDb.delete(DatabaseOpenHelper.REGISTRATIONS_TABLE_NAME, "registrationId = ?", new String[] { registrationId });
    }
}
