package com.lese.androidcallnotifier;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 *
 * Created by Matti Määttä on 23.7.2014.
 */
public class DevicesFragment extends Fragment {

    private InstallationIdProvider mIdProvider;
    private RegistrationsProvider mRegistrations;
    private SQLiteDatabase mDb;
    private ListView mListView;
    private View mNoDevicesView;
    private Button mSendButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DatabaseOpenHelper dbHelper = new DatabaseOpenHelper(getActivity());
        mDb = dbHelper.getWritableDatabase();
        mRegistrations = new RegistrationsProvider(mDb);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.devices_view, container, false);

        mIdProvider = new InstallationIdProvider(getActivity().getSharedPreferences(Globals.PREFS_NAME, 0));

        final NotificationFactory notificationFactory = new NotificationFactory(mIdProvider);

        mNoDevicesView = view.findViewById(R.id.devicesEmptyView);

        mSendButton = (Button)view.findViewById(R.id.button);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject message = notificationFactory.create(
                        "Android Call Notifier Test",
                        "Message received!",
                        "Android Call Notifier, " + android.os.Build.MODEL,
                        "default");

                MessagingService messagingService = MessagingServiceProvider.create(mDb);
                messagingService.send(message, 30);
            }
        });

        mListView = (ListView)view.findViewById(R.id.devices_list_view);
        populateListView();

        return view;
    }

    public void populateListView() {
        mListView.setAdapter(new RegistrationsAdapter(mRegistrations.getRegistrations()));

        if (mListView.getAdapter().getCount() > 0) {
            mSendButton.setEnabled(true);
            mNoDevicesView.setVisibility(View.INVISIBLE);
        } else {
            mSendButton.setEnabled(false);
            mNoDevicesView.setVisibility(View.VISIBLE);
        }
    }

    private class RegistrationsAdapter extends ArrayAdapter<JSONObject> {
        public RegistrationsAdapter(List<JSONObject> items) {
            super(getActivity(), R.layout.item_registration, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final JSONObject item = getItem(position);

            // Check if an existing view is being reused
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_registration, parent, false);
            }

            // Get text views
            TextView viewName = (TextView)convertView.findViewById(R.id.deviceNameView);
            TextView viewId = (TextView)convertView.findViewById(R.id.registrationIdView);

            // Set delete button action
            ImageButton button = (ImageButton)convertView.findViewById(R.id.remove_device_button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    remove(item);

                    if (getCount() == 0) {
                        mNoDevicesView.setVisibility(View.VISIBLE);
                        mSendButton.setEnabled(false);
                    }

                    try {
                        String registrationId = item.getString("registration_id");
                        mRegistrations.removeRegistration(registrationId);

                        // Send revoke authorization message
                        MessagingService messagingService = MessagingServiceProvider.create(registrationId);
                        JSONObject revokeMessage = new JSONObject();
                        revokeMessage.put("revoke_authorization", mIdProvider.getInstallationId());
                        messagingService.send(revokeMessage, 24 * 60 * 60);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            try {
                viewId.setText(item.getString("registration_id").substring(1, 30) + "..");
                viewName.setText(item.getString("device"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }
}
