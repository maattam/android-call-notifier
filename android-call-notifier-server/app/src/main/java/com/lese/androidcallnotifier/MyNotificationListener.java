package com.lese.androidcallnotifier;

import android.content.Intent;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

/**
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public class MyNotificationListener extends NotificationListenerService {
    private static final String TAG = "NotificationListener";

    public static final String INTENT_ACTION = "com.lese.androidcallnotifier.NOTIFICATION_LISTENER_ACTION";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "NotificationListener onCreate");
    }

    @Override
    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        Intent intent = new Intent(INTENT_ACTION);
        intent.putExtra("notification_event", "onNotificationPosted");
        intent.putExtra("packageName", statusBarNotification.getPackageName());

        if (statusBarNotification.getNotification().tickerText != null) {
            intent.putExtra("tickerText", statusBarNotification.getNotification().tickerText.toString());
        }

        Log.v(TAG, "Received notification with id " + statusBarNotification.getId()
                + " from package " + statusBarNotification.getPackageName());

        intent.putExtras(statusBarNotification.getNotification().extras);
        sendBroadcast(intent);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification statusBarNotification) {
    }
}
