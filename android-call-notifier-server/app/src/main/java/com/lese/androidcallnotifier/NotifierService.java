package com.lese.androidcallnotifier;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Background service for forwarding telephony events to cloud messaging service.
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public class NotifierService extends Service {
    private static String TAG = "NotifierService";

    private boolean mStarted = false;
    private NotificationFactory mNotificationFactory;
    private ContactNameProvider mContactNameProvider;
    private SQLiteDatabase mDb;

    private List<BroadcastReceiver> mBroadcastReceivers = new ArrayList<>();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int startType = Service.START_STICKY;

        if (mStarted) {
            return startType;
        }

        mStarted = true;
        Log.v(TAG, "NotifierService started");

        InstallationIdProvider idProvider = new InstallationIdProvider(getSharedPreferences(Globals.PREFS_NAME, 0));
        mNotificationFactory = new NotificationFactory(idProvider);

        DatabaseOpenHelper helper = new DatabaseOpenHelper(this);
        mDb = helper.getReadableDatabase();
        mContactNameProvider = new ContactNameProvider(this);

        // Start listening for incoming phone calls
        createPhoneCallListener();

        // Start receiving sms
        createSmsReceiver();

        // Start receiving status bar notifications
        createNotificatonReceiver();

        return startType;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        for (BroadcastReceiver receiver : mBroadcastReceivers) {
            unregisterReceiver(receiver);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createPhoneCallListener() {
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(new PhoneCallListener(new PhoneCallDelegate() {
            @Override
            public void ringing(String incomingNumber) {
                String title = incomingNumber;
                String messageBody = "";

                String contactName = mContactNameProvider.getContactName(incomingNumber);
                if (contactName != null) {
                    title = contactName;
                    messageBody = incomingNumber;
                }

                // Format notification
                JSONObject message = mNotificationFactory.create(
                        title,
                        messageBody,
                        android.os.Build.MODEL,
                        "call");

                MessagingServiceProvider.create(mDb).send(message, 120);
            }
        }), PhoneStateListener.LISTEN_CALL_STATE);
    }

    private void createSmsReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");

        BroadcastReceiver smsReceiver = new SmsReceiver(new SmsReceiverDelegate() {
            @Override
            public void receive(String phoneNumber, String messageBody) {
                String title = phoneNumber;

                String contactName = mContactNameProvider.getContactName(phoneNumber);
                if (contactName != null) {
                    title = contactName + " " + phoneNumber;
                }

                // Format notification
                JSONObject message = mNotificationFactory.create(
                        title,
                        messageBody,
                        android.os.Build.MODEL,
                        "sms");

                MessagingServiceProvider.create(mDb).send(message, 15 * 60);
            }
        });

        mBroadcastReceivers.add(smsReceiver);
        registerReceiver(smsReceiver, filter);
    }

    private void createNotificatonReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(MyNotificationListener.INTENT_ACTION);

        NotifiersProvider notifiersProvider = new NotifiersProvider(mDb);
        BroadcastReceiver notificationReceiver = new NotificationReceiver(notifiersProvider, new NotificationReceiverDelegate() {
            @Override
            public void receive(String title, String text, String subText, String iconEncoded, String packageName) {
                JSONObject json = mNotificationFactory.create(
                        title,
                        text,
                        subText + ", " + android.os.Build.MODEL,
                        packageName);

                if (iconEncoded != null) {
                    try {
                        json.put("iconData", iconEncoded);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                MessagingServiceProvider.create(mDb).send(json, 15 * 60);
            }
        });

        mBroadcastReceivers.add(notificationReceiver);
        registerReceiver(notificationReceiver, filter);
    }
}
