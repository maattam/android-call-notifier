package com.lese.androidcallnotifier;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 *
 * Created by Matti on 28/01/2015.
 */
public class NotifierServiceLauncher extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
         // Start background service
        Intent serviceIntent = new Intent(context, NotifierService.class);
        context.startService(serviceIntent);
    }
}
