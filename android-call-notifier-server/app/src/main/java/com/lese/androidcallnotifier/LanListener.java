package com.lese.androidcallnotifier;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 *
 * Created by Matti Määttä on 19.7.2014.
 */
public class LanListener {
    private static final String TAG = "LanListener";

    private ListenDelegate mDelegate;

    private Thread mListenThread;
    private ListenRunnable mRunnable;

    public void setDelegate(ListenDelegate delegate) {
        mDelegate = delegate;
    }

    /**
     * Starts listening for JSONObjects.
     */
    synchronized public void start()
    {
        if (mListenThread != null) {
            return;
        }

        mRunnable = new ListenRunnable();
        mListenThread = new Thread(mRunnable);
        mListenThread.start();
    }

    /**
     * Stops listenin.
     */
    synchronized public void stop()
    {
        if (mListenThread == null) {
            return;
        }

        try {
            mRunnable.close();
            mListenThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mRunnable = null;
        mListenThread = null;
    }

    private class ListenRunnable implements Runnable {
        DatagramSocket mSocket;

        @Override
        public void run() {
            byte[] buffer = new byte[4096];

            try {
                mSocket = new DatagramSocket(Globals.LISTEN_PORT);

                while (!mSocket.isClosed()) {
                    Log.v(TAG, "Listening..");

                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    mSocket.receive(packet);

                    try {
                        JSONObject json = new JSONObject(new String(buffer, "UTF-8"));
                        mDelegate.receive(json);

                    } catch (JSONException e) {
                        Log.v(TAG, "Received non-json packet");
                    }
                }
            }

            catch (SocketException e) {
                // Closed socket
            }

            catch (Exception e) {
                e.printStackTrace();
            }

            Log.v(TAG, "Stopped ListenRunnable");
            mSocket = null;
        }

        public void close() {
            if (mSocket != null) {
                mSocket.close();
            }
        }
    }
}
