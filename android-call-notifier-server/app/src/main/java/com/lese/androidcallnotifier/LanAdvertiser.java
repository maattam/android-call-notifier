package com.lese.androidcallnotifier;

import android.util.Log;

import org.json.JSONObject;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * Sends advertisement packets over UDP multicast.
 *
 * Created by Matti Määttä on 19.7.2014.
 */
public class LanAdvertiser {
    private static final String TAG = "LanAdvertiser";

    private static final int ADVERTISE_DELAY = 5000;

    private final JSONObject mAdvertisement;

    private Thread mMulticastThread;
    private MultiCastRunnable mRunnable;

    public LanAdvertiser(JSONObject advertisement)
    {
        mAdvertisement = advertisement;
    }

    /**
     * Starts advertising the attached advertisement.
     */
    synchronized public void start()
    {
        if (mMulticastThread != null) {
            return;
        }

        mRunnable = new MultiCastRunnable();
        mMulticastThread = new Thread(mRunnable);
        mMulticastThread.start();
    }

    /**
     * Stops advertising.
     */
    synchronized public void stop()
    {
        if (mMulticastThread == null) {
            return;
        }

        mRunnable.mRunning = false;
        mMulticastThread.interrupt();

        try {
            mMulticastThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mRunnable = null;
        mMulticastThread = null;
    }

    private class MultiCastRunnable implements Runnable {
        boolean mRunning = true;

        @Override
        public void run() {
            Log.v(TAG, "MultiCastRunnable starting to advertise");

            try {
                byte[] payload = mAdvertisement.toString().getBytes("UTF-8");

                MulticastSocket socket = new MulticastSocket();
                socket.joinGroup(InetAddress.getByName(Globals.ADVERTISE_GROUP));
                socket.setTimeToLive(Globals.TTL);

                while (mRunning) {
                    Log.v(TAG, "Advertising " + mAdvertisement.toString());

                    DatagramPacket packet = new DatagramPacket(payload, payload.length,
                            InetAddress.getByName(Globals.ADVERTISE_GROUP), Globals.ADVERTISE_PORT);
                    socket.send(packet);

                    Thread.sleep(ADVERTISE_DELAY);
                }
            }

            catch (InterruptedException e) {
                // Closing..
            }

            catch (Exception e) {
                e.printStackTrace();
            }

            Log.v(TAG, "Stopped MultiCastRunnable");
        }
    }
}
