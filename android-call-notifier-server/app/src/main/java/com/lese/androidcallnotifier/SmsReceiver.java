package com.lese.androidcallnotifier;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * BroadcastReceiver for receiving SMS messages.
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public class SmsReceiver extends BroadcastReceiver {
    private final SmsReceiverDelegate mDelegate;

    public SmsReceiver(SmsReceiverDelegate delegate) {
        mDelegate = delegate;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.provider.Telephony.SMS_RECEIVED")) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                try {
                    for (Object pdus : (Object[])bundle.get("pdus")) {
                        SmsMessage message = SmsMessage.createFromPdu((byte[])pdus);
                        mDelegate.receive(message.getOriginatingAddress(), message.getMessageBody());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
