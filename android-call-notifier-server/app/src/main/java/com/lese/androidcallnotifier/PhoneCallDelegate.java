package com.lese.androidcallnotifier;

/**
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public interface PhoneCallDelegate {
    void ringing(String incomingNumber);
}
