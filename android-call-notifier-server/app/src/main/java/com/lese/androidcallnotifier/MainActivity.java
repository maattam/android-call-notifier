package com.lese.androidcallnotifier;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends FragmentActivity {
    private static final String TAG = "MainActivity";

    private static final String APP_ID = "c037d506-546f-43f9-9889-5df2fb2b52db";

    private LanAdvertiser mAdvertiser;
    private LanListener mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create tabs
        final FragmentTabHost tabHost = (FragmentTabHost)findViewById(R.id.tabHost);
        tabHost.setup(this, getSupportFragmentManager(), R.id.tab_content);

        tabHost.addTab(tabHost.newTabSpec("tabDevices").setIndicator("Devices"), DevicesFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("tabApps").setIndicator("Apps"), AppsFragment.class, null);

        // Create multicast lock
        WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        WifiManager.MulticastLock multicastLock = wifiManager.createMulticastLock("AndroidCallNotifierLock");
        multicastLock.acquire();

        InstallationIdProvider idProvider = new InstallationIdProvider(getSharedPreferences(Globals.PREFS_NAME, 0));

        // Create multicast advertiser
        JSONObject advertisement = new JSONObject();
        try {
            advertisement.put("app_id", APP_ID);
            advertisement.put("device", android.os.Build.MODEL);
            advertisement.put("installation_id", idProvider.getInstallationId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create advertiser
        mAdvertiser = new LanAdvertiser(advertisement);

        DatabaseOpenHelper dbHelper = new DatabaseOpenHelper(this);
        final RegistrationsProvider registrations = new RegistrationsProvider(dbHelper.getWritableDatabase());

        // Create listener
        mListener = new LanListener();
        mListener.setDelegate(new ListenDelegate() {
            @Override
            public void receive(final JSONObject data) {
                Log.v(TAG, "Received " + data.toString());

                try {
                    if (!data.getString("app_id").equals(APP_ID)) {
                        Log.w(TAG, "Wrong app id");
                    } else if (registrations.addRegistration(data)) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DevicesFragment fragment = (DevicesFragment)getSupportFragmentManager().findFragmentByTag("tabDevices");
                                if (fragment != null) {
                                    fragment.populateListView();
                                }
                            }
                        });
                    }

                } catch (JSONException e) {
                    Log.w(TAG, "Received invalid JSON");
                }
            }
        });

        // Start background service
        Intent serviceIntent = new Intent(this, NotifierService.class);
        startService(serviceIntent);
    }

    @Override
    public void onResume() {
        super.onResume();

        mAdvertiser.start();
        mListener.start();
    }

    @Override
    public void onPause() {
        super.onPause();

        mAdvertiser.stop();
        mListener.stop();
    }
}
