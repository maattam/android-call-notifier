package com.lese.androidcallnotifier;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Simple one-way Google Cloud Messaging server implementation.
 *
 * Created by Matti Määttä on 19.7.2014.
 */
public class CloudMessagingService implements MessagingService {
    private static final String TAG = "CloudMessagingService";

    private final String mApiKey;
    private final List<String> mRegistrationIds = new ArrayList<>();

    public CloudMessagingService(String apiKey)
    {
        mApiKey = apiKey;
    }

    /**
     * Adds a registered GCM receiver.
     */
    public void addReceiver(String registrationId) {
        mRegistrationIds.add(registrationId);
    }

    @Override
    public void send(JSONObject data, int ttl) {
        if (mRegistrationIds.isEmpty()) {
            return;
        }

        new PostMessageTask(UUID.randomUUID().toString(), ttl).execute(data);
    }

    private class PostMessageTask extends AsyncTask<JSONObject, Void, HttpResponse> {
        private final String mId;
        private final int mTTL;

        public PostMessageTask(String id, int ttl)
        {
            mId = id;
            mTTL = ttl;
        }

        @Override
        protected HttpResponse doInBackground(JSONObject... jsonObjects) {
            JSONObject data = jsonObjects[0];
            HttpClient httpClient = new DefaultHttpClient();

            try {
                HttpPost httpPost = new HttpPost("https://android.googleapis.com/gcm/send");
                httpPost.setHeader("Authorization", "key=" + mApiKey);
                httpPost.setHeader("Content-Type", "application/json");

                JSONArray registrationIds = new JSONArray();
                for (String id : mRegistrationIds) {
                    registrationIds.put(id);
                }

                JSONObject request = new JSONObject();
                request.put("registration_ids", registrationIds);
                request.put("message_id", mId);
                request.put("time_to_live", mTTL);
                request.put("data", data);

                httpPost.setEntity(new StringEntity(request.toString(), "UTF-8"));
                return httpClient.execute(httpPost);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(HttpResponse response) {
            if (response != null) {
                Log.i(TAG, "PostMessageTask status: " + response.getStatusLine().toString());
            } else {
                Log.w(TAG, "PostMessageTask failed");
            }
        }
    }

}
