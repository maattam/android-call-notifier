package com.lese.androidcallnotifier;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;

/**
 * Receiver for receiving status bar notifications.
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public class NotificationReceiver extends BroadcastReceiver {
    private static final String TAG = "NotificationReceiver";

    private final NotificationReceiverDelegate mDelegate;
    private final NotifiersProvider mProvider;

    public NotificationReceiver(NotifiersProvider provider, NotificationReceiverDelegate delegate) {
        mDelegate = delegate;
        mProvider = provider;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals(MyNotificationListener.INTENT_ACTION)) {
            return;
        }

        Bundle bundle = intent.getExtras();

        try {
            String packageName = bundle.getString("packageName");

            // Filter certain apps, ie. "android"
            if (!canAcceptNotification(packageName)) {
                return;
            }

            String title = bundle.getString("android.title");
            CharSequence subTextCs = bundle.getString("android.subText");
            CharSequence textCs = bundle.getString("android.text");
            String iconEncoded = null;

            if (title == null) {
                title = bundle.getString("tickerText");
            }

            Bitmap iconBitmap = (Bitmap)bundle.getParcelable("android.largeIcon");
            if (iconBitmap == null) {
                // Try smaller icon, if this fails we could get the application icon
                iconBitmap = (Bitmap)bundle.getParcelable("android.icon");
            }

            if (iconBitmap != null) {
                String base64Image = createEncodedIcon(iconBitmap);

                // GCM message can only fit 4k worth of data
                if (base64Image.length() < 3800) {
                    Log.w(TAG, "Notification icon size is " + base64Image.length() + ", ignoring.");
                    iconEncoded = base64Image;
                }
            }

            String text = "";
            String subText = "";

            if (textCs != null) {
                text = textCs.toString();
            }

            if (subTextCs != null) {
                subText = subTextCs.toString();
            } else {
                // Get application name, or package name
                subText = getApplicationName(context, packageName);
                if (subText == null) {
                    subText = packageName;
                }
            }

            mDelegate.receive(title, text, subText, iconEncoded, packageName);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String createEncodedIcon(Bitmap bitmap) {
        Bitmap scaledIcon = Bitmap.createScaledBitmap(bitmap, 80, 80, true);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        scaledIcon.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);

        return "data:image/jpg;base64," + Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
    }

    private String getApplicationName(Context context, String packageName) {
        PackageManager packageManager = context.getPackageManager();
        String applicationName;

        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 0);
            applicationName = packageManager.getApplicationLabel(applicationInfo).toString();

        } catch (Exception e) {
            applicationName = null;
        }

        return applicationName;
    }

    private boolean canAcceptNotification(String packageName) {
        // Disable android system notifications
        if (packageName.equals("android")) {
            return false;
        }

        return mProvider.doesNotify(packageName);
    }
}
