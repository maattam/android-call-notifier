package com.lese.androidcallnotifier;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Matti Määttä on 24.7.2014.
 */
public class NotifiersProvider {
    final private SQLiteDatabase mDb;

    public NotifiersProvider(SQLiteDatabase db) {
        mDb = db;
    }

    /**
     * Forwards notifications originating from this package name.
     */
    public void addNotifier(String packageName) {
        ContentValues row = new ContentValues();
        row.put("packageName", packageName);

        mDb.insert(DatabaseOpenHelper.NOTIFIERS_TABLE_NAME, null, row);
    }

    /**
     * Removes notification forwarding from this package name.
     */
    public void removeNotifier(String packageName) {
        mDb.delete(DatabaseOpenHelper.NOTIFIERS_TABLE_NAME, "packageName = ?", new String[] { packageName });
    }

    /**
     * Returns all forwarders.
     */
    public List<String> getNotifiers() {
        List<String> notifiers = new ArrayList<>();

        Cursor cursor = mDb.query(DatabaseOpenHelper.NOTIFIERS_TABLE_NAME,
                new String[] { "packageName" },
                null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                ContentValues row = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, row);

                notifiers.add(row.getAsString("packageName"));

            } while (cursor.moveToNext());
        }

        cursor.close();
        return notifiers;
    }

    public boolean doesNotify(String packageName) {
        Cursor cursor = mDb.rawQuery("SELECT * FROM " + DatabaseOpenHelper.NOTIFIERS_TABLE_NAME + " WHERE packageName=?",
                new String[] { packageName });

        boolean notifies = cursor.moveToFirst();
        cursor.close();
        return notifies;
    }
}
