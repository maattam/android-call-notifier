package com.lese.androidcallnotifier;

import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Fetches registrations from database and adds them to instantiated CloudMessagingService.
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public class MessagingServiceProvider {
    public static MessagingService create(SQLiteDatabase db) {
        CloudMessagingService service = new CloudMessagingService(Globals.GCM_API_KEY);

        RegistrationsProvider provider = new RegistrationsProvider(db);
        for (JSONObject registration : provider.getRegistrations()) {
            try {
                service.addReceiver(registration.getString("registration_id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return service;
    }

    public static MessagingService create(String receiverId) {
        CloudMessagingService service = new CloudMessagingService(Globals.GCM_API_KEY);
        service.addReceiver(receiverId);
        return service;
    }
}
