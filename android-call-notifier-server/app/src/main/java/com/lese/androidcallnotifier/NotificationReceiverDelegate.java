package com.lese.androidcallnotifier;

/**
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public interface NotificationReceiverDelegate {
    void receive(String title, String text, String subText, String iconEncoded, String packageName);
}
