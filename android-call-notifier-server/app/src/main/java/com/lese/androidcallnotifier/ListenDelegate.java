package com.lese.androidcallnotifier;

import org.json.JSONObject;

/**
 *
 * Created by Matti Määttä on 19.7.2014.
 */
public interface ListenDelegate {
    void receive(JSONObject data);
}
