package com.lese.androidcallnotifier;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

/**
 * Uses PhoneLookup to retrieve contact name for phone number.
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public class ContactNameProvider {
    private final Context mContext;

    public ContactNameProvider(Context context) {
        mContext = context;
    }

    public String getContactName(String phoneNumber) {
        ContentResolver contentResolver = mContext.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = contentResolver.query(uri, new String[] {ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }

        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        cursor.close();
        return contactName;
    }
}
