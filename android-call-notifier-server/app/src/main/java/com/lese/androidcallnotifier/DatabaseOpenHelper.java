package com.lese.androidcallnotifier;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Provides SQLite database with registrations table for storing registration ids.
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public class DatabaseOpenHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "androidcallnotifier";

    public static final String REGISTRATIONS_TABLE_NAME = "registrations";
    public static final String NOTIFIERS_TABLE_NAME = "notifiers";

    public DatabaseOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + REGISTRATIONS_TABLE_NAME + " (_id INTEGER PRIMARY KEY, registrationId TEXT UNIQUE, device TEXT);");
        db.execSQL("CREATE TABLE " + NOTIFIERS_TABLE_NAME + " (_id INTEGER PRIMARY KEY, packageName TEXT UNIQUE);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
    }
}
