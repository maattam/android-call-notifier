package com.lese.androidcallnotifier;

import android.content.SharedPreferences;
import android.util.Log;

import java.util.UUID;

/**
 * Retrieves installation id from shared preferences, or generated a new one if none exist.
 *
 * Created by Matti Määttä on 20.7.2014.
 */
public class InstallationIdProvider {
    private static final String TAG = "InstallationProvider";
    private static final String SETTINGS_KEY = "installationId";

    private SharedPreferences mSettings;

    public InstallationIdProvider(SharedPreferences settings) {
        mSettings = settings;
    }

    public String getInstallationId() {
        if (!mSettings.contains(SETTINGS_KEY)) {
            String id = UUID.randomUUID().toString();
            Log.v(TAG, "Created new installation id " + id);

            SharedPreferences.Editor editor = mSettings.edit();
            editor.putString(SETTINGS_KEY, id);
            editor.commit();
        }

        return mSettings.getString(SETTINGS_KEY, "");
    }
}
