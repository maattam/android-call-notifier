### Android Call Notifier ###

Android Call Notifier forwards Android application bar notifications, text messages and incoming phone call notifications to registered Google Chrome devices.
The system consist of Android client *android-call-notifier-server* and Google Chrome app *android-call-notifier-chrome*.

Communication between Android and Google Chrome devices is implemented using UDP and Google Cloud Messaging API. To pair Android and Chrome together, make sure both devices
are in the same WLAN network so Chrome receives UDP broadcast messages from Android.

## Configuration ##

You need Google Cloud Messaging public API key and client ID from Google Developer Console.

- Paste the public API key to *android-call-notifier-server/app/src/main/java/com/lese/androidcallnotifier/Globals.java*
- Paste the private client ID to *android-call-notifier-chrome/background.js*
